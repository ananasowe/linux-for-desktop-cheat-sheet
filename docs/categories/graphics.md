# Graphics

## Nvidia

Disable flipping (prevents tearing during OBS recording):  
`nvidia-settings --assign AllowFlipping=0`

Turning the flipping back on:  
`nvidia-settings --assign AllowFlipping=1`
