# SSH

## Tunneling

Forward local port 8181 to remote 80  
`ssh -L 8181:192.168.0.10:80 user@server`

Socks proxy (via remote server) on local port 8181  
`ssh -D 8181 user@server`

Reverse tunneling from remote port 8181 to local 8080  
`ssh -R 8181:localhost:8080 user@server`

For reverse tunneling to work, following options must be set in `/etc/ssh/sshd_config`   
```
AllowTcpForwarding yes
GatewayPorts yes
```

