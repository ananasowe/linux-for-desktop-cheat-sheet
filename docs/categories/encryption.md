# Encryption

## GnuPG

### Managing keys

Export a public key:  
`gpg --output public.key --armor --export <key>`

Export a private key:  
`gpg --output private.key --armor --export-secret-key <key>`

Import a key:  
`gpg --import public.key`