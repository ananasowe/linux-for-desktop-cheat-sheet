# Package managers

## Pacman
Unused packages can be removed with
`pacman -Rsn $(pacman -Qdtq)`

## Flatpak
By default, all apps installed by flatpak are placed in either `/var/lib/flatpak/app/` (system-wide) or `~/.var/app/` (user) folders.
Either of those two can be selected by adding `--user` or `--system` argument.

Applications are installed with  
`flatpak install APP-ID`

and uninstalled with  
`flatpak uninstall APP-ID`

Unused runtimes can be removed with  
`flatpak uninstall --unused`

Selected runtimes can be removed with  
`flatpak uninstall --runtime RUNTIME-ID`

### Permissions

Each app has its default permissions declared in the manifest file  
(`/var/lib/flatpak/app/[APP-ID]/current/active/file/manifest.json` or `~/.var/app/[APP-ID]/current/active/file/manifest.json`)

They can be overridden with a file  
`/var/lib/flatpak/overrides/[APP-ID]`
(Notice the file doesn't have any extensions)

Filesystem permissions (filesystems) include:

| Permission | Directory (Directories) |
| ------- | --------- |
| host | Access to all files |
| home | Access to home directory |
| xdg-desktop | Access to XDG desktop directory | |
| xdg-documents | Access the XDG documents directory |
| xdg-download | Access the XDG download directory |
| xdg-music | Access the XDG music directory |
| xdg-pictures | Access the XDG pictures directory |
| xdg-public-share | Access the XDG public directory |
| xdg-videos | Access the XDG videos directory |
| xdg-templates | Access the XDG templates directory |
| xdg-config | Access the XDG config directory |
| xdg-cache | Access the XDG cache directory |
| xdg-data | Access the XDG data directory |
| /some-directory/ | Access to some-directory |
| ~/some-directory/ | Access to some-directory relative to user home path |

The following options can be added after filesystem permissions:

| Option | Access |
| ------ | ------ |
| :ro | Read-only |
| :rw | Read and write (default) |
| :create | Read, write and directory creation (if it doesn't exist) |

Shared resources permissions (shared) include:

| Permission | Resource |
| ---------- | -------- |
| ipc | Share IPC namespace with the host |
| network | Share network access |

Socket permissions (socket) include:

| Permission | Socket |
| ---------- | ------ |
| x11 | Allow creating X11 windows |
| wayland | Allow creating wayland windows |
| pulseaudio | Allow access to the sound server |

Feature permissions (feature) include:

| Permission | Feature |
| ---------- | ------- |
| bluetooth | Bluetooth access |
| multiarch | Allow running 32-bit binaries provided with the package |

For example, an override configuration file that would give read-only access to `/var` directory, full access to the user downloads directory, access to bluetooth, but block network access would look like this:
```
[Context]
filesystems=xdg-download;/var/:ro;
shared=!network;
features=bluetooth;
```
