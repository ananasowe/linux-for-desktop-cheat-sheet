# Networking

## Manual network configuration

Show IP addresses:  
`ip addr show`

Add a static IP address:  
`ip addr add 192.168.1.1/24 dev eth0`

Remove a static IP address:  
`ip addr del 192.168.1.1/24 dev eth0`

Show routing table:
`ip route show`

Add an entry to the routing table:  
`ip route add default via 192.168.1.1 dev eth0` - default route via gateway 192.168.1.1 that can be reached on eth0  
or  
`ip route add 192.168.1.0/24 via 192.168.1.1` - route to 192.168.1.0 via gateway 192.168.1.1  
or  
`ip route add 192.168.1.0/24 dev eth0` - route to 192.168.1.0 that can be reached on eth0

Remove an entry from the routing table:  
`ip route delete 192.168.1.0/24 via 192/158.1.1`

Bring an interface up or down:  
`ip link set eth0 up`  
`ip link set eth0 down`

List open network sockets:  
`lsof -i -n -P`
